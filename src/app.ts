import * as express from 'express';
import * as bodyParser from 'body-parser';
import Database from './database';
import Movie from './models/Movie';
import Actor from './models/Actor';
import { movieRoutes } from './routes/routes';

class App {
	
	app: express.Application;
	private db;

	constructor() {
		this.app = express();
		this.db = new Database();
		this.config();
		this.app.use(movieRoutes);
	}

	private config(): void {
		this.app.use(bodyParser.json());
		this.app.use(bodyParser.urlencoded({ extended: false }));
	}
}

export default new App().app;
