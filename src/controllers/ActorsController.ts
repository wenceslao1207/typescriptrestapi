import { Request, Response } from 'express';
import Movie from '../models/Movie';
import Actor from '../models/Actor';

export class ActorController {
	
	public async findAll(req: Request, res: Response) {
		try { 
			const actors = await Actor.findAll({
				include: [{ model: Movie }]
			});
			if (actors !== null) {
				res.json(actors);
			} else {
				res.status(404).send({ error: "No actors found" });
			}
		} catch (err) {
			res.status(500).send({ error: err });
		}
	}

	public async findOne(req: Request, res: Response) {
		try {
			const id = req.params.id;
			const actor = await Actor.findOne({
				where:  {
					id: id
				},
				include: [{ model: Movie }]
			});
			if (actor !==  null) {
				res.json(actor);
			} else {
				res.status(400).send({ error: "No Actor found "});
			}
		} catch (err) {
			res.status(500).send({ error: err });
		}
	}
}

export const actorController = new ActorController();
