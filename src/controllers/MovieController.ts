import { Request, Response } from 'express';
import Movie from '../models/Movie';
import Actor from '../models/Actor';

export class MovieController {

	public async findAll(req: Request, res: Response) {
		try { 
			const movies = await Movie.findAll({
				include: [{ model: Actor }]
			});
			if (movies !== null) {
				res.json(movies);
			} else {
				res.status(404).send({ error: "No movies found" });
			}
		} catch (err) {
			res.status(500).send({ error: err });
		}
	}

	public async findByName(req: Request, res: Response) {
		const name: string = req.params.name;
		const movie = await Movie.findOne({
			where: {
				name: name
			},
			include: [{ model: Actor }]
		});
		if (movie != null) {
			res.json(movie)
		} else {
			res.status(404).send({ error: "Movies not found"})
		}
	}

	public async findOne(req: Request, res:Response) {
		try {
			const id = req.params.id;
			const movie = await Movie.findOne({
				where: {
					id: id
				},
				include: [{ model: Actor }]
			})
			if (movie != null) {
				res.json(movie);
			} else {
				res.status(404).send({ error: " Movie not found" });
			}
		} catch (err) {
			console.log(err)
			res.status(500).send({ message: err })
		}
	}
}

export const movieController = new MovieController();
