import { Sequelize } from 'sequelize-typescript';
import Movie from './models/Movie';
import Actor from './models/Actor';
import ActorsMovie from './models/ActorsMovies';

class Database {

	private sequelize: Sequelize;

	constructor() {
		try {
			this.sequelize = new Sequelize({
				database: 'laravel',
				dialect: 'mariadb',
				username: 'root',
				password: 'Lordlao-1207|Wenceslao',
				storage: ':memory:',
				modelPaths: [__dirname + './models'],
				logging: false
			});

		} catch (err) {
			console.log(err);
		}
		this.sequelize.addModels([Movie, Actor, ActorsMovie]);
	}
}

export default Database;
