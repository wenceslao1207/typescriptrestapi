import { Table, Column, Model, BelongsToMany } from 'sequelize-typescript';
import Movie from './Movie';
import ActorsMovie from './ActorsMovies';

@Table({
	modelName: 'actors',
	timestamps: false,
})
class Actor extends Model<Actor> {

	@Column
	get name(): string {
		return this.getDataValue('name');
	}

	@Column
	get lastname(): string {
		return this.getDataValue('lastname');
	}

	@BelongsToMany(() => Movie, () => ActorsMovie)
	movies: Array<Movie & {ActorsMovie: ActorsMovie}>;
}

export default Actor;
