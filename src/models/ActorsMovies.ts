import { Table, Column, Model, ForeignKey }  from 'sequelize-typescript';
import Actor from './Actor';
import Movie from './Movie';

@Table({
	tableName: 'actors_movie',
	modelName: 'actors_movie',
	timestamps: false,
})
class ActorsMovie extends Model<ActorsMovie> {

	@ForeignKey(() => Actor)
	@Column({
		field: 'actors_id'
	})
	actorId: number;


	@ForeignKey(() => Movie)
	@Column({
		field: 'movie_id',
	})
	movieId: number;
}

export default ActorsMovie;
