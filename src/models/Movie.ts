import { Table, Column, Model, BelongsToMany } from 'sequelize-typescript';
import Actor from './Actor';
import ActorsMovie from './ActorsMovies';

@Table({
	modelName: 'movies',
	timestamps: false,
})
class Movie extends Model<Movie> {

	
	@Column
	get name(): string {
		return this.getDataValue('name');	
	}

	@Column
	get genere(): string {
		return this.getDataValue('genere');
	}

	@Column
	get description(): string{
		return this.getDataValue('description');
	}
		
	@BelongsToMany(() => Actor, () => ActorsMovie)
	actors: Array<Actor & {ActorsMovie: ActorsMovie}>;

}

export default Movie;
