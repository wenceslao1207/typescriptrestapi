import { Request, Response, Router } from 'express';
import { movieController } from '../controllers/MovieController';
import { actorController } from '../controllers/ActorsController';

class MovieRoutes {
	public router: Router = Router();

	constructor() {
		this.config();
	}

	private config(): void {
		
		// Movie Routes
		this.router.get('/movies', (req: Request, res: Response) => {
			movieController.findAll(req, res);
		});
		this.router.get('/movies/:id', (req: Request, res: Response) => {
			movieController.findOne(req, res);
		});
		this.router.get('/movies/name/:name', (req: Request, res: Response) => {
			movieController.findByName(req, res);
		});

		//Actors routes
		this.router.get('/actors', (req: Request, res: Response) => {
			actorController.findAll(req, res);
		});

		this.router.get('/actors/:id', (req: Request, res: Response) => {
			actorController.findOne(req, res)
		});
	}
}

export const movieRoutes = new MovieRoutes().router;
