import app from "./app";

const PORT = process.env.PORT || 8030;

	app.listen(PORT, () => {
		console.log("APP running on port + ", PORT);
	});
